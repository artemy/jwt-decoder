GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
BINARY_NAME=jwt-decoder
GOARCH=amd64
PLATFORMS=linux darwin windows

.PHONY: clean build all $(addprefix build_,$(PLATFORMS))
.SILENT: clean

all: clean build

build: $(addprefix build_,$(PLATFORMS))
$(addprefix build_,$(PLATFORMS)): build_%:
	GOOS=$(patsubst build_%,%,$@) GOARCH=$(GOARCH) $(GOBUILD) -v -o $(BINARY_NAME)-$(patsubst build_%,%,$@)

clean:
	$(GOCLEAN)
	find . -name '${BINARY_NAME}[-?][a-zA-Z0-9]*' -maxdepth 1 -delete