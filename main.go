package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"os"
	"strings"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Please provide jwt")
		return
	}
	tokenString := os.Args[1]
	printToken(tokenString)
}

func printToken(tokenString string) {
	tokenArray := strings.Split(tokenString, ".")
	printBanner("HEADER")
	printPart(tokenArray[0])
	printBanner("CLAIMS")
	printPart(tokenArray[1])
	printBanner("MISC")
}

func printBanner(name string) {
	stars := strings.Repeat("*", 10)
	fmt.Printf("\n%s %s %s\n", stars, name, stars)
}

func printPart(part string) {
	if val, err := decodeTokenPart(part); err == nil {
		if output, err := prettyPrintJSONBytes(val); err == nil {
			fmt.Println(output)
		}
	}
}

func prettyPrintJSONBytes(input []byte) (string, error) {
	var out bytes.Buffer
	if err := json.Indent(&out, input, "", "\t"); err != nil {
		return "", err
	}
	return out.String(), nil
}

func decodeTokenPart(part string) ([]byte, error) {
	decoded, err := base64.RawURLEncoding.DecodeString(part)
	if err != nil {
		return nil, err
	}
	return decoded, nil
}
